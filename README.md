# Heathfield High School GPA Calculator
Calculates GPA for Heathfield High School reports. From version 0.1.0 onwards it can calculate any number of grades (assignments/multiple reports).

Changes:
v1.1.0:
- Removed redundant "how many classes do you have" selector
- Custom domain set up; https now working
- Changed wording from "Calculate" to " Enter the grades from either your report or assignments to calculate your GPA"
- Added Reset button

v1.0.0:
- Moved to gitlab (github blocked at school)
- Hosted website on gitlab pages (now available at https://gpa.joshnewman6.com or https://gpacalculator.joshnewman6.com)
- Updated look of website 
    - everything now centered
    - nicepage css
- Added gitlab link to website footer

v0.1.2:
- Fixed bug where numbers would get divided by 2 when only one number entered

v0.1.1
- Updated calculator to divide by op.value instead of ce.value

v0.1.0:
- Added support for amounts of classes other than 7 or 8

v0.0.2:
- Fixed lesson selector incorrectly stating all year 9s have 7 classes

v0.0.1:
- GPA calculator created

Future:
- Add information about how many classees each year level has
- Make website look better
- Add support for grades lower than D- and place grades D+ and lower in a drop-down menu
- removed unused css in nicepage.css

[Email](mailto:Josh.Newman429@schools.sa.edu.au?subject=Heathfield%20HS%20GPA%20Calculator)
